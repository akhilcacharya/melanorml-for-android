package com.akhilcacharya.melanorml.Models;

import com.akhilcacharya.melanorml.Models.MelanormlQuery;
import com.akhilcacharya.melanorml.Models.MelanormlResult;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;


public interface MelanormlService {
    @Headers({"Authorization: Basic IWXa9jnI4zMvPQ7CuS0GwkbHk40aOsmyWTpFh1qkirfIlWyIty"})
    @POST("/vision/classify")
    void result(@Body MelanormlQuery query, Callback<MelanormlResult> cb);
}
