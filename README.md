#Melanorml

Early detection for melanoma using mobile devices

##App Screens
![App Screens](https://bytebucket.org/akhilcacharya/melanorml-for-android/raw/dbdd2f20624f7f389eedcaf6891dd250163f4754/demo/Screens.png?token=ff1692eb63c9f0b3712cfc55cd1b2e86599b6868)

##Demo Video
Video may be found [here](https://www.youtube.com/embed/J6HgB2uLf6M).

##Slide Deck
Slides can be found [here](https://docs.google.com/presentation/d/1KWWua1R3g4If4Sazgls8uGM5KEeL75cZn2ZkIc77QQs/edit?usp=sharing)

##License
The MIT License (MIT)

Copyright (c) 2015 Akhil Acharya

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
